# Supported tags and respective Dockerfile links

- [`latest`](https://bitbucket.org/mikolajr/docker-squid/src/master/Dockerfile)

For more information about this image and its history, please see [its source repository](https://bitbucket.org/mikolajr/docker-squid). 
This image is automagically built and updated via pull requests.
Only the most recent version of Squid is being maintained.

# What is Squid?

[Squid](http://www.squid-cache.org/) is a caching proxy for the Web supporting HTTP, HTTPS, FTP, and more.
I use it for privacy protection, because government and internet provider do capture internet traffic metadata. 
When configured to accept HTTPS (TODO) proxy requests they won't be able to know what you are looking at.

Squid supports such configuration, unfortunately modern browsers do not. At least without hacking.
https://wiki.squid-cache.org/Features/HTTPS?highlight=%28faqlisted.yes%29#Encrypted_browser-Squid_connection

# How to use this image

```console
docker run --rm -d -p 3128:3128 mikolajr/docker-squid
```

And your proxy server is available at localhost on port 3128.

# Image details

HTTP port: `3128`

# Contributing

You are invited to contribute new features, fixes, or updates, large or small.
